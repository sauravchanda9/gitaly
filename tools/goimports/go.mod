module gitlab.com/gitlab-org/gitaly/tools/goimports

go 1.21

require golang.org/x/tools v0.19.0

require golang.org/x/mod v0.16.0 // indirect
